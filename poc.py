from sklearn import cluster
import numpy
import PyQt5

def get_structured_data(file_path):
    with open(file_path, "r") as data:
        lines = data.readlines()
        data_list = []
        for line in lines:
            splitted_data = line.split(" ")
            date = splitted_data [0]
            time = splitted_data[1]
            lat = splitted_data[2]
            lng = splitted_data[3]
            data_list.append({
                "date": date,
                "time": time,
                "lat": lat,
                "lng": lng
            })
    return data_list



def get_clusters():
    #Fill an array of lat_lngs from the file
    data_list = get_structured_data("location.dat")
    length = len(data_list)
    lat_lng_list = [[float(data['lat']), float(data['lng'])] for data in data_list]

    initialized_array = numpy.empty(shape=(length, 2), dtype=float)
    #FIll initialized numpy array with lat_lngs for k-means fitting
    count = 0
    for lat_lng in lat_lng_list:
        initialized_array[count] = lat_lng
        count += 1

    #Run the algorithm with K=4
    k_means = cluster.KMeans(n_clusters=4)
    k_means_fitted = k_means.fit(initialized_array)

    #labels is a list that containts the cluster index of every lat_lng that was in initalized_array, with the same sequence
    #So now we will create a dictionary in this format :
    """
    {
        "clusterIndex" : [
            lat_lng1,
            lat_lng2,
            lat_lng3
        ]
    }

    """
    labels = k_means_fitted.labels_
    clusters = {}
    count = 0
    for label in labels:
        if clusters.get(label) == None:
            clusters[label] =[]
        clusters[label].append(initialized_array[count])
        count += 1
    return clusters


# This code is going to be used for finding the best K for k-means
# total_inertia = 0
# k_max = 30
# for i in range(1, k_max):
#     k_means = cluster.KMeans(n_clusters=i)
#     k_means_fitted = k_means.fit(initialized_array)
#
#     inertia = k_means_fitted.inertia_
#     total_inertia += inertia
#
# average_inertia = float(total_inertia) / k_max
# thresold = average_inertia / k_max
# last_inertia = 0
# first_inertia = True
# k = 0
# for i in range(1, k_max):
#     k_means = cluster.KMeans(n_clusters=i)
#     k_means_fitted = k_means.fit(initialized_array)
#
#     inertia = k_means_fitted.inertia_
#     if first_inertia:
#         last_inertia = inertia
#         first_inertia = False
#     else:
#         print last_inertia - inertia
#         if last_inertia - inertia < thresold:
#             k = i - 1
