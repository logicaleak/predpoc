import sys
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from PyQt4.QtWebKit import *

import poc
import time

class LocationDataForWeb(QObject):
    def __init__(self, parent=None):
        super(LocationDataForWeb, self).__init__(parent)

    @pyqtSlot(str)
    def text(self, message):
        print message


html = """
    <!DOCTYPE html>
        <html>
          <head>
            <style type="text/css">
              html, body { height: 100%; margin: 0; padding: 0; }
              #map { height: 100%; }
            </style>
          </head>
          <body>
            <div id="map"></div>
            <script type="text/javascript">

                var map;
                function initMap() {
                  map = new google.maps.Map(document.getElementById('map'), {
                    center: {lat: 41.061747, lng: 28.996676},
                    zoom: 12
                  });
                }



            </script>
            <script
              src="https://maps.googleapis.com/maps/api/js?callback=initMap&v=3.exp&sensor=true">
            </script>
            <script>
                var locationList = [];
                function appendToLocationList(latLng) {
                    locationList.push(latLng);
                }

                function addMarkersToMap() {
                    var marker = new google.maps.Marker({
                        position: locationList[0],
                        map: map,
                        title: 'location'
                    });

                }
            </script>

          </body>
        </html>
    """

class WebPage(QWebPage):
    def javaScriptConsoleMessage(self, msg, line, source):
        print '%s line %d: %s' % (source, line, msg)


class Web(QMainWindow):
    def __init__(self, parent=None):
        super(Web, self).__init__(parent)

        self.view = QWebView()

        page = WebPage()
        print "hey"
        self.view.setHtml(html)
        self.view.setPage(page)

        self.frame = self.view.page().mainFrame()

    def loadFinished(self, ok):
        print "Loaded"

def main():
    app = QApplication(sys.argv)

    web = Web()
    web.show()

    app.exec_()

    # cluster_list = poc.get_clusters()
    # for cluster in cluster_list:
    #     location_list = cluster_list[cluster]
    #     for location in location_list:
    #         lat_str = location[0].__str__()
    #         lng_str = location[1].__str__()
    #         append_location_js_code = """appendToLocationList({lat : """ + lat_str + """, lng :""" + lng_str + """ })"""
    #         frame.evaluateJavaScript(append_location_js_code)
    #
    # add_markers_js_code = """addMarkersToMap()"""
    # frame.evaluateJavaScript(add_markers_js_code)




if __name__ == "__main__":
    main()