import sys
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from PyQt4.QtWebKit import *
import numpy
import poc

html = """
    <!DOCTYPE html>
        <html>
          <head>
            <style type="text/css">
              html, body { height: 100%; margin: 0; padding: 0; }
              #map { height: 100%; }
            </style>
          </head>
          <body>
            <div id="map"></div>
            <script type="text/javascript">

                var map;
                var googleLoaded = false;
                function initMap() {
                  map = new google.maps.Map(document.getElementById('map'), {
                    center: {lat: 41.061747, lng: 28.996676},
                    zoom: 12
                  });
                  googleLoaded = true;
                }

                var locationList = [];
                function appendToLocationList(latLng) {
                    locationList.push(latLng);
                }

                function addMarkersToMap() {
                    if (googleLoaded) {
                        locationList.forEach(function(location) {
                            var marker = new google.maps.Marker({
                                position: location,
                                map: map,
                                title: 'location'
                            });
                        });

                    } else {
                        setTimeout(addMarkersToMap, 1000);
                    }


                }

                function drawRectangle(minX, maxY, maxX, minY) {
                    if (googleLoaded) {

                        var rectangle = new google.maps.Rectangle({
                            strokeColor: '#FF0000',
                            strokeOpacity: 0.8,
                            strokeWeight: 2,
                            fillColor: '#FF0000',
                            fillOpacity: 0.35,
                            map: map,
                            bounds: new google.maps.LatLngBounds(
                                new google.maps.LatLng(minX, minY),
                                new google.maps.LatLng(maxX, maxY))
                        });

                    } else {
                        console.log("no");
                        setTimeout(function() {
                            drawRectangle(minX, maxY, maxX, minY);
                        }, 1000);
                    }

                }



            </script>
            <script
              src="https://maps.googleapis.com/maps/api/js?callback=initMap&v=3.exp&sensor=true">
            </script>
            <script>

            </script>

          </body>
        </html>
    """

#This page is needed to let javascript consoles flooded back to python code
class WebPage(QWebPage):
    def javaScriptConsoleMessage(self, msg, line, source):
        print '%s line %d: %s' % (source, line, msg)



def main():
    app = QApplication(sys.argv)
    window = QWebView()

    page = WebPage()

    window.setPage(page)
    window.setHtml(html)

    #Page above is needed to obtain this. This is the interface that lets us run javascript code in the web page
    #evaluateJavaScript runs the given String code. However, it is not sequential. Google maps is almsot always not loaded
    #until our evaluates run, so I have given setTimeouts for this to work. For further details ask me :)
    frame = window.page().mainFrame()

    cluster_list = poc.get_clusters()
    rectangle_list = []
    for cluster in cluster_list:
        location_list = cluster_list[cluster]
        #Find the maximums and minimums of x and y for rectangle bounds
        min_x, min_y = numpy.min(location_list, axis=0)
        max_x, max_y = numpy.max(location_list, axis=0)

        #create the rectangle data for this cluster
        rectangle = [min_x, max_y, max_x, min_y]
        rectangle_list.append(rectangle)
        for location in location_list:
            lat_str = location[0].__str__()
            lng_str = location[1].__str__()

            #Append the location into the javascript locationList, to create the markers, later
            append_location_js_code = """appendToLocationList({lat : """ + lat_str + """, lng :""" + lng_str + """ })"""
            frame.evaluateJavaScript(append_location_js_code)

    #Create markers. WARNING: THIS FUNCTION IS QUITE WEIRD IN JAVASCRIPT. ASK OZUM FOR DETAILS
    add_markers_js_code = """addMarkersToMap()"""
    frame.evaluateJavaScript(add_markers_js_code)

    #Draw all the rectangles defined for each clusters. AGAIN, THIS IS QUITE WEIRD, ASK OZUM FOR DETAILS
    for rectangle in rectangle_list:
        rectangle_code = "drawRectangle(" + rectangle[0].__str__() + "," + rectangle[1].__str__() + "," + rectangle[2].__str__() + "," + rectangle[3].__str__() + ")"
        print rectangle_code
        frame.evaluateJavaScript(rectangle_code)

    window.move(200, 200)
    window.show()


    app.exec_()

if __name__ == "__main__":
    main()